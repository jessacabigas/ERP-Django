from django.forms import ModelForm
from Erpnext.models import person

class AddUserForm(ModelForm):
	class Meta:
		model = person
		fields = ['fname', 'lname', 'email']
