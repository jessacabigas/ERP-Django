from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class person(models.Model):
	fname = models.CharField(max_length=20, null=True, blank = True)
	lname = models.CharField(max_length=20, null=True, blank = True)
	email = models.CharField(max_length=30 , null = True, blank = True)
	# username = models.CharField(max_length=20, null=True, blank = True)

	def __str__(self):
		return str(self.fname) + " " + str(self.lname)
