from django.conf.urls import url
from Erpnext.views import *
from . import views

urlpatterns = [
	url(r'^register$', RegistrationView.as_view(), name = 'register'),
	url(r'^login$', LoginView.as_view(), name = 'login'),

]
