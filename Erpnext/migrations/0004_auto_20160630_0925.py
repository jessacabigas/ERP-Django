# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-30 09:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Erpnext', '0003_auto_20160630_0744'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='user_id',
        ),
        migrations.AddField(
            model_name='person',
            name='email',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='username',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
